﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProductionControlActualData.MVC.Controller;
using ProductionControlActualData.DataAccess;

namespace ProductionControlActualData.MVC.Task
{
    public class ShiftChanger : Runnable
    {
        private equipos selectedShift;
        private ShiftChangeController controller;
        public ShiftChanger(MainController mvcController, equipos selectedShift) : base(mvcController)
        {
            this.selectedShift = selectedShift;
            this.controller = mvcController;
        }

        public override void StopTask()
        {

        }

        protected override void RunResult()
        {
            this.controller.CloseShiftView();
        }

        protected override void RunTask()
        {
            ShiftDao.Instance.ChangeShift(this.selectedShift);
            MachineDataDao.Instance.ChangeShift(this.selectedShift);
            TaskCompleted();
        }
    }
}
