﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProductionControlActualData.MVC.Controller;
using ProductionControlActualData.DataManage;
using ProductionControlActualData.DataAccess;
using ProductionControlActualData.DataManage.StopReason;
using ProductionControlActualData.DataManage.ItemData;

namespace ProductionControlActualData.MVC.Task
{
    public class GetStopReasonData : Runnable
    {
        private LoomViewData viewData;
        private StopChangeController controller;
        private StopReasonViewData data;
        public GetStopReasonData(MainController mvcController, LoomViewData viewData) : base(mvcController)
        {
            this.controller = mvcController;
            this.viewData = viewData;
            this.data = null;
        }

        public override void StopTask()
        {
        }

        protected override void RunResult()
        {
            if (this.data != null)
                if (this.data.AllstopTypes != null && this.data.SelectedStop != null)
                {
                    foreach (TiposParo stopType in this.data.AllstopTypes)
                    {
                        stopType.Descripcion = stopType.Descripcion.Trim();
                    }
                    controller.InitializeView(this.data);
                }
        }

        protected override void RunTask()
        {
            if (this.viewData != null)
            {
                this.data = new StopReasonViewData(StopTypeDao.Instance.AllStops);
                List<Matricula> allItems= ItemDataDao.Instance.ItemsForGroup;
                this.data.ItemNumbers = new List<ItemDataForView>();
                foreach (Matricula item in allItems)
                    this.data.ItemNumbers.Add(new ItemDataForView(item));
                int selectedValue = -1;
                if (Int32.TryParse(this.viewData.StopReasonId, out selectedValue))
                {
                    this.data.SelectedStop = StopTypeDao.Instance.StopById(selectedValue);
                    this.data.IsWoChange = false;
                    if (this.viewData.WoLot != null)
                        this.data.WoLot = this.viewData.WoLot.Trim();
                    if (this.viewData.WoNbr != null)
                        this.data.WoNbr = this.viewData.WoNbr.Trim();
                }
            }
            TaskCompleted();
        }
    }
}
