﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProductionControlActualData.MVC.Controller;
using ProductionControlActualData.DataManage.StopReason;
using ProductionControlActualData.DataAccess;
using ProductionControlActualData.DataManage.ItemData;

namespace ProductionControlActualData.MVC.Task
{
    public class WorkOrderChecker : Runnable
    {
        private StopChangeController controller;
        private StopReasonViewData data;
        private WO_MSTR foundWo;
        public WorkOrderChecker(MainController mvcController, StopReasonViewData data) : base(mvcController)
        {
            this.controller = mvcController;
            this.data = data;
            this.foundWo = null;
        }

        public override void StopTask()
        {
        }

        protected override void RunResult()
        {
            if (this.foundWo != null)
            {
                this.data.WoLot = this.foundWo.wo_lot; 
                this.data.WoNbr = this.foundWo.wo_nbr;
                this.data.SelectedItemNumber = this.data.ItemNumbers.Find(item => item.ItemNumber.Trim().ToUpper() == this.foundWo.wo_part.Trim().ToUpper());
                if (this.foundWo.wo_qty_ord != null)
                    this.data.WoOrderedQty = (double)this.foundWo.wo_qty_ord;
                else
                    this.data.WoOrderedQty = 0;
                this.controller.InitializeView(this.data);
            }
        }

        protected override void RunTask()
        {
            if (this.data != null)
            {
                if (this.data.WoLot != null)
                    if (this.data.WoLot.Trim().Length > 0)
                        this.foundWo = WorkOrderDao.Instance.GetByWoId(this.data.WoLot.Trim());
                if (this.foundWo == null)
                    if (this.data.WoNbr != null)
                        if (this.data.WoNbr.Trim().Length > 0)
                            this.foundWo = WorkOrderDao.Instance.GetByWoNbr(this.data.WoNbr.Trim());
            }
            TaskCompleted();
        }
    }
}
