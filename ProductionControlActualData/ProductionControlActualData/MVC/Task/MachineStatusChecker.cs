﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProductionControlActualData.MVC.Controller;
using ProductionControlActualData.DataAccess;
using ProductionControlActualData.DataManage.Machine;
using System.Threading;

namespace ProductionControlActualData.MVC.Task
{
    public class MachineStatusChecker : Runnable
    {
        private MainControllerView controller;
        private bool stopTask;
        public MachineStatusChecker(MainController mvcController) : base(mvcController)
        {
            this.controller = mvcController;
            this.stopTask = false;
        }

        public override void StopTask()
        {
            this.stopTask = true;
        }

        protected override void RunResult()
        {

        }

        protected override void RunTask()
        {
            while (!this.stopTask)
            {
                if (controller.MachinesVisible)
                {
                    List<MachineStatus> machines = MachineStatusDao.Instance.GetMachineStatus();
                    this.controller.MachineDataUpdated(machines);
                    Thread.Sleep(Properties.Settings.Default.MachineStatusRefreshTime);
                }
                else
                    Thread.Sleep(Properties.Settings.Default.EndlessLoopTimeOut);
            }
            TaskCompleted();
        }
    }
}
