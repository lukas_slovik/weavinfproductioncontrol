﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProductionControlActualData.MVC.Controller;
using ProductionControlActualData.DataAccess;

namespace ProductionControlActualData.MVC.Task
{
    public class ShiftChangeDataLoader : Runnable
    {
        private ShiftChangeController controller;
        private List<equipos> shiftData;
        public ShiftChangeDataLoader(MainController mvcController) : base(mvcController)
        {
            this.controller = mvcController;
            this.shiftData = null;
        }

        public override void StopTask()
        {
        }

        protected override void RunResult()
        {
            if (this.shiftData != null)
            {
                this.controller.InitializeShiftData(this.shiftData);
            }
        }

        protected override void RunTask()
        {
            this.shiftData = ShiftDao.Instance.AllShifts;
            TaskCompleted();
        }
    }
}
