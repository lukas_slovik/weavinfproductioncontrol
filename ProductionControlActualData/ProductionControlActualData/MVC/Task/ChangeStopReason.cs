﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProductionControlActualData.MVC.Controller;
using ProductionControlActualData.DataManage.StopReason;
using ProductionControlActualData.DataManage;
using ProductionControlActualData.DataAccess;

namespace ProductionControlActualData.MVC.Task
{
    public class ChangeStopReason : Runnable
    {
        private StopChangeController controller;
        private StopReasonViewData viewData;
        private LoomViewData machineViewData;
        private bool successful;
        public ChangeStopReason(MainController mvcController, StopReasonViewData viewData, LoomViewData machineViewData) : base(mvcController)
        {
            this.machineViewData = machineViewData;
            this.viewData = viewData;
            this.successful = false;
            this.controller = mvcController;
        }

        public override void StopTask()
        {
        }

        protected override void RunResult()
        {
            if (this.successful)
                this.controller.CloseStopView();
        }

        private void NoWoUpdate()
        {
            string currentWo = MachineDataDao.Instance.GetCurrentWo(this.machineViewData.LoomId);
            string newWo = "";
            if (currentWo != null)
            {
                newWo += Properties.Settings.Default.NoWorkOrderIndentificator + currentWo;
                if (newWo.Length > 10)
                    newWo = newWo.Substring(0, 10);
            }
            else
            {
                Random rnd = new Random();
                int month = rnd.Next(1, 999999);
                newWo += Properties.Settings.Default.NoWorkOrderIndentificator + Properties.Settings.Default.NoWorkOrderIndentificator + month.ToString();
            }
            MachineDataDao.Instance.AddNewWO(this.machineViewData.LoomId,
                newWo,
                this.viewData.SelectedItemNumber.ItemNumber,
                "NOWONBR",
                this.viewData.WoOrderedQty,
                this.viewData.SelectedStopId);
        }

        protected override void RunTask()
        {
            if (viewData != null && machineViewData != null)
            {
                if (this.viewData.SelectedItemNumber != null)
                {
                    if (MachineDataDao.Instance.IsNoWoState(this.machineViewData.LoomId))
                    {
                        if (this.viewData.WoLot != null)
                        {
                            if (this.viewData.WoLot.Trim().Length > 0)
                            {
                                WorkOrderMachineDataDao.Instance.WorkOrderEntered(MachineDataDao.Instance.GetCurrentWo(this.machineViewData.LoomId),
                                    this.viewData.WoLot,
                                    this.viewData.SelectedItemNumber.ItemNumber,
                                    this.viewData.WoOrderedQty);
                                MachineDataDao.Instance.AddNewWO(this.machineViewData.LoomId,
                                    this.viewData.WoLot,
                                    this.viewData.SelectedItemNumber.ItemNumber,
                                    WoDataOperations.WoParentLot(this.viewData.WoLot),
                                this.viewData.WoOrderedQty,
                                    this.viewData.SelectedStopId);
                                this.successful = true;
                            }
                        }
                    }
                    else
                    {
                        if (this.viewData.WoLot != null)
                        {
                            if (this.viewData.WoLot.Trim().Length > 0)
                                MachineDataDao.Instance.AddNewWO(this.machineViewData.LoomId,
                                    this.viewData.WoLot,
                                    this.viewData.SelectedItemNumber.ItemNumber,
                                     WoDataOperations.WoParentLot(this.viewData.WoLot),
                                    this.viewData.WoOrderedQty,
                                    this.viewData.SelectedStopId);
                            else
                                NoWoUpdate();
                        }
                        else
                            NoWoUpdate();
                        this.successful = true;
                    }
                }
                else
                {
                    MachineDataDao.Instance.UpdateMachineStopReason(this.machineViewData.LoomId, this.viewData.SelectedStopId);
                    this.successful = true;
                }
            }
            TaskCompleted();
        }
    }
}
