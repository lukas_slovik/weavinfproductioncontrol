﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProductionControlActualData.MVC.Controller;
using ProductionControlActualData.DataAccess;
using ProductionControlActualData.DataManage.StopReason;

namespace ProductionControlActualData.MVC.Task
{
    public class StopReasonCheck : Runnable
    {
        private StopChangeController controller;
        private StopReasonViewData stopData;
        public StopReasonCheck(MainController mvcController, StopReasonViewData stopData) : base(mvcController)
        {
            this.controller = mvcController;
            this.stopData = stopData;
        }

        public override void StopTask()
        {
        }

        protected override void RunResult()
        {
            this.controller.InitializeView(this.stopData);
        }

        protected override void RunTask()
        {
            if (stopData != null)
            {
                stopData.IsWoChange = WoChangeStopType.IsWoChange(stopData.SelectedStopId);
                stopData.WoLot = "";
                stopData.WoNbr = "";
            }
            TaskCompleted();
        }
    }
}
