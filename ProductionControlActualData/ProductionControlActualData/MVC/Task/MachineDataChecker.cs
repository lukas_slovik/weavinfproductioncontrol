﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProductionControlActualData.MVC.Controller;
using System.Threading;
using ProductionControlActualData.DataManage.Machine;
using ProductionControlActualData.DataAccess;
using ProductionControlActualData.DataManage;

namespace ProductionControlActualData.MVC.Task
{
    public class MachineDataChecker : Runnable
    {
        private MainControllerView controller;
        private bool stopTask;
        public MachineDataChecker(MainController mvcController) : base(mvcController)
        {
            this.controller = mvcController;
            this.stopTask = false;
        }

        public override void StopTask()
        {
            this.stopTask = true;
        }

        protected override void RunResult()
        {

        }

        protected override void RunTask()
        {
            while (!this.stopTask)
            {
                LoomViewData viewData = new LoomViewData();
                ProduccionActual actualData = MachineDataDao.Instance.GetMachineData(MachineIdConverter.Convert(this.controller.MachineId));
                if (actualData != null)
                {
                    List<Produccion> woData = WorkOrderMachineDataDao.Instance.ByWoId(actualData.OrdenProduccion);
                    CambioTurno lastShiftChange = ShiftChangeDao.Instance.LastShift();
                    viewData.LoomId = MachineIdConverter.Convert(this.controller.MachineId);
                    viewData.StatusText = MachineState.CheckStateTextView(actualData.Estado);
                    viewData.IsRunning = MachineState.CheckState(actualData.Estado);
                    viewData.StatusColor = Properties.Settings.Default.StoppedStatusColor;
                    if (viewData.IsRunning != null)
                        if ((bool)viewData.IsRunning)
                            viewData.StatusColor = Properties.Settings.Default.RunningStatusColor;
                    if (actualData.CodigoParo != null)
                        viewData.StopReasonId = actualData.CodigoParo.ToString();
                    else
                        viewData.StopReasonId = "";
                    viewData.StopReasonDesc = StopTypeDao.Instance.StopDescriptionById(actualData.CodigoParo);
                    if (actualData.Velocidad != null)
                        viewData.Speed = (int)actualData.Velocidad;
                    else
                        viewData.Speed = 0;
                    if (actualData.Pasadas != null)
                        viewData.Peaks = (int)actualData.Pasadas;
                    else
                        viewData.Peaks = 0;
                    if (actualData.MetrosFabricados != null)
                        viewData.LastRunMeters = (double)actualData.MetrosFabricados;
                    else
                        viewData.LastRunMeters = 0;
                    viewData.ItemNumber = actualData.Articulo;
                    viewData.ItemDesc = ItemDataDao.Instance.ItemDescription(actualData.Articulo);
                    viewData.HasWo = actualData.OrdenProduccion != null;
                    viewData.WoNbr = WoDataOperations.WoNbr(actualData.OrdenProduccion);
                    viewData.WoLot = WoDataOperations.WoLot(actualData.OrdenProduccion);
                    viewData.WoMeters = (double)actualData.LongitudPieza;
                    viewData.MetersProduced = WoDataOperations.MetersProduced(woData);
                    viewData.MetersLeft = viewData.WoMeters - viewData.MetersProduced;
                    if (viewData.MetersLeft < 0)
                        viewData.MetersLeft = 0;
                    viewData.WoStart = WoDataOperations.WoStart(woData);
                    DateTime probableEnd = WoDataOperations.ProbableEnd(woData, actualData);
                    viewData.WoProbableEnd = probableEnd.ToString();
                    viewData.WoWorkingHours = WoDataOperations.WoWorkingHours(woData);
                    viewData.WoProbableLeftHours = WoDataOperations.WoWorkingHoursLeft(probableEnd);
                    if (lastShiftChange != null)
                    {
                        viewData.ShiftId = lastShiftChange.Equipo;
                        viewData.ShiftStartDate = lastShiftChange.FechaHora.ToString();
                    }
                    controller.MachineDataUpdated(viewData);
                }
                Thread.Sleep(Properties.Settings.Default.LoomViewDataRefresh);
            }
            TaskCompleted();
        }
    }
}
