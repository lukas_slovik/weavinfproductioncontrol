﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProductionControlActualData.View;
using ProductionControlActualData.MVC.Task;
using ProductionControlActualData.DataManage.Machine;
using ProductionControlActualData.DataManage;
using ProductionControlActualData.DataAccess;
using ProductionControlActualData.DataManage.StopReason;
using ProductionControlActualData.View.ShiftChange;

namespace ProductionControlActualData.MVC.Controller
{
    public class MainController : ControllerTasksCore, MainControllerView, StopChangeController, ShiftChangeController
    {
        private LoomViewData viewData;
        private MainViewControl view;
        private ChangeStopViewMode stopReasonView;
        private ShiftChangeViewMode shiftChangeView;
        private ValueChanger woIdChanges;
        private ValueChanger woNbrChanges;
        public MainController()
        {
            this.view = null;
            this.viewData = null;
            this.stopReasonView = null;
            this.shiftChangeView = null;
            this.woIdChanges = null;
            this.woNbrChanges = null;
            MachinesVisible = false;
            MachineId = Properties.Settings.Default.InitialMachineId;
        }
        public void Test()
        {


        }
        #region MainControllerView interface implementation
        public string MachineId { get; private set; }
        public bool MachinesVisible { get; set; }
        public void CloseView()
        {
            this.view.CloseView();
            StopController();
        }

        public void MachineDataUpdated(LoomViewData viewData)
        {
            this.viewData = viewData;
            if (this.view != null)
                this.view.InitializeView(viewData);
        }

        public void MachineDataUpdated(List<MachineStatus> machines)
        {
            if (this.view != null)
                this.view.MachineStatusUpdated(machines);
        }

        public void MachineIdChanged(string machineId)
        {
            MachineId = machineId;
        }

        public void ViewLoaded(MainViewControl view)
        {
            this.view = view;
            AddTask(new MachineStatusChecker(this));
            AddTask(new MachineDataChecker(this));
        }

        public void ChangeStopReason()
        {
            this.view.ShowChangeStopView(this);
        }

        public void ChangeShift()
        {
            this.view.ShowShiftChangeView(this);
        }
        #endregion
        #region StopChangeController interface implementation 
        public void ChangeStopViewLoaded(ChangeStopViewMode view)
        {
            this.stopReasonView = view;
            AddTask(new GetStopReasonData(this, this.viewData));
        }

        public void ViewClosed()
        {
            if (this.view != null)
                this.view.ReturnToMainView();
        }

        public void InitializeView(StopReasonViewData viewData)
        {
            if (this.stopReasonView != null)
                this.stopReasonView.InitializeView(viewData);
        }

        public void StopReasonChanged(StopReasonViewData selectedReason)
        {
            AddTask(new StopReasonCheck(this, selectedReason));
        }
        private void WorkOrderInfoChanged(object resultValue)
        {
            AddTask(new WorkOrderChecker(this, resultValue as StopReasonViewData));
        }

        public void WorkOrderIdChanged(StopReasonViewData value)
        {
            if (this.woIdChanges == null)
                woIdChanges = new ValueChanger(WorkOrderInfoChanged);
            else if (this.woIdChanges.StopChanger)
                woIdChanges = new ValueChanger(WorkOrderInfoChanged);
            woIdChanges.ValueChange(value);
        }

        public void WorkOrderNbrChanged(StopReasonViewData value)
        {
            if (this.woNbrChanges == null)
                woNbrChanges = new ValueChanger(WorkOrderInfoChanged);
            else if (this.woNbrChanges.StopChanger)
                woNbrChanges = new ValueChanger(WorkOrderInfoChanged);
            woNbrChanges.ValueChange(value);
        }

        public void StopReasonDataComfirmed(StopReasonViewData viewData)
        {
            AddTask(new ChangeStopReason(this, viewData, this.viewData));
        }

        public void CloseStopView()
        {
            if (this.stopReasonView != null)
                this.stopReasonView.CloseView();
            ReturnToMainView();
        }
        #endregion
        #region ShiftChangeController interface implementation
        public void CloseShiftView()
        {
            if (this.shiftChangeView != null)
                this.shiftChangeView.CloseView();
            if (this.view != null)
                this.view.ReturnToMainView();
        }

        public void ReturnToMainView()
        {
            if (this.view != null)
                this.view.ReturnToMainView();
        }

        public void ShiftViewLoaded(ShiftChangeViewMode view)
        {
            this.shiftChangeView = view;
            AddTask(new ShiftChangeDataLoader(this));
        }

        public void InitializeShiftData(List<equipos> allShifts)
        {
            if (this.shiftChangeView != null)
                this.shiftChangeView.InitializeView(allShifts);
        }

        public void ShiftSelected(equipos selectedShift)
        {
            if (selectedShift != null)
            {
                AddTask(new ShiftChanger(this, selectedShift));
            }
        }
        #endregion
    }
}
