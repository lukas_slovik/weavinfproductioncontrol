﻿using ProductionControlActualData.DataManage;
using ProductionControlActualData.DataManage.Machine;
using ProductionControlActualData.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductionControlActualData.MVC.Controller
{
    public interface MainControllerView
    {
        bool MachinesVisible { get; set; }
        string MachineId { get;}
        void ViewLoaded(MainViewControl view);
        void CloseView();
        void MachineDataUpdated(List<MachineStatus> machines);
        void MachineDataUpdated(LoomViewData viewData);
        void MachineIdChanged(string machineId);
        void ChangeStopReason();
        void ChangeShift();
    }
}
