﻿using ProductionControlActualData.DataAccess;
using ProductionControlActualData.DataManage.StopReason;
using ProductionControlActualData.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductionControlActualData.MVC.Controller
{
    public interface StopChangeController
    {
        void ChangeStopViewLoaded(ChangeStopViewMode view);
        void ViewClosed();
        void InitializeView(StopReasonViewData viewData);
        void StopReasonChanged(StopReasonViewData selectedReason);
        void WorkOrderIdChanged(StopReasonViewData viewData);
        void WorkOrderNbrChanged(StopReasonViewData viewData);
        void StopReasonDataComfirmed(StopReasonViewData viewData);
        void CloseView();
        void CloseStopView();
    }
}
