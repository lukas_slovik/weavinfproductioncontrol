﻿using ProductionControlActualData.DataAccess;
using ProductionControlActualData.View.ShiftChange;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductionControlActualData.MVC.Controller
{
    public interface ShiftChangeController
    {
        void CloseShiftView();
        void ShiftViewLoaded(ShiftChangeViewMode view);
        void InitializeShiftData(List<equipos> allShifts);
        void ShiftSelected(equipos selectedShift);
    }
}
