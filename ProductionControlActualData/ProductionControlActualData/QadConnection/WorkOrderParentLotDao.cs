﻿using ProductionControlActualData.DataAccess;
using ProductionControlActualData.DataManage.WorkOrder;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductionControlActualData.QadConnection
{
    public class WorkOrderParentLotDao
    {
        public static List<WoAlloc> GetWoAllocations(string woId)
        {
            List<WoAlloc> woDataAllocations = new List<WoAlloc>();
            if (woId != null)
            {
                DataTable woAllocations = ODBCconnection.Instance.ExecuteODBCcommand("SELECT lad_nbr, lad_part,lad_lot,lad_ref FROM PUB.lad_det WHERE lad_nbr='" + woId.Trim() + "'");
                if (woAllocations != null)
                {
                    foreach (DataRow woRow in woAllocations.Rows)
                    {
                        woDataAllocations.Add(new WoAlloc(woRow));
                    }
                }
            }
            return woDataAllocations;
        }

        public static string GetLot(string woId)
        {
            List<WoAlloc> woallocations = GetWoAllocations(woId);
            foreach (WoAlloc pieceAlloc in woallocations)
            {
                if (ItemDataDao.Instance.ItemGroup(pieceAlloc.ItemNumber.Value) == Properties.Settings.Default.ParentItemGroup)
                    return pieceAlloc.LotSerial.Value;
            }
            return "";
        }
    }
}
