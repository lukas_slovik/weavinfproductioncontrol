﻿using ProductionControlActualData.InfoHandle;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProductionControlActualData.QadConnection
{
    public class ODBCconnection
    {
        private static ODBCconnection instance;

        public static ODBCconnection Instance
        {
            get
            {
                if (ODBCconnection.instance == null)
                    ODBCconnection.instance = new ODBCconnection();
                return ODBCconnection.instance;
            }
        }
        
        private int activeConnectionCount;
        private object connectionLock;
        private ODBCconnection()
        {
            this.activeConnectionCount = 0;
            this.connectionLock = new object();
        }

        private bool CanConnect
        {
            get
            {
                lock (this.connectionLock)
                {
                    if (this.activeConnectionCount < Properties.Settings.Default.MaxOdbcConnectionCount)
                        return true;
                    return false;
                }
            }
        }

        private void ConnectionStart()
        {
            lock (this.connectionLock)
            {
                this.activeConnectionCount++;
            }
        }

        private void ConnectionStop()
        {
            lock (this.connectionLock)
            {
                this.activeConnectionCount--;
            }
        }

        private DataTable ExecuteQuery(string command)
        {
            DataTable resulttemtable = new DataTable();
            try
            {
                ConnectionStart();
                using (var mfgCon = new OdbcConnection("DRIVER=" + Properties.Settings.Default.QadDatabaseDriver +
                    ";host=" + Properties.Settings.Default.QadDatabaseHost + 
                    ";uid=" + Properties.Settings.Default.QadDatabaseUid + 
                    "; password=" + Properties.Settings.Default.QadDatabasePassword + 
                    "; port=" + Properties.Settings.Default.QadDatabasePort + 
                    ";db=" + Properties.Settings.Default.QadDatabaseName + ";"))
                {
                    mfgCon.Open();
                    OdbcTransaction transaction = mfgCon.BeginTransaction(IsolationLevel.ReadUncommitted);
                    using (var commandOdb = new OdbcCommand(command, mfgCon, transaction))
                    {
                        DataSet dataSet = new DataSet();
                        dataSet.EnforceConstraints = false;
                        OdbcDataReader reader = commandOdb.ExecuteReader();
                        dataSet.Load(reader, LoadOption.OverwriteChanges, "ResultTable");
                        resulttemtable = dataSet.Tables["ResultTable"];
                    }
                    transaction.Commit();
                }
            }
            catch (Exception exc)
            {
                EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), exc);
            }
            finally
            {
                ConnectionStop();
            }
            return resulttemtable;
        }

        public DataTable ExecuteODBCcommand(string command)
        {
            while (!CanConnect)
                Thread.Sleep(Properties.Settings.Default.EndlessLoopTimeOut);
            return ExecuteQuery(command);
        }

    }
}
