﻿using ProductionControlActualData.DataAccess;
using ProductionControlActualData.DataManage.Machine;
using ProductionControlActualData.QadConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductionControlActualData.DataManage
{
    public class WoDataOperations
    {
        public static double MetersProduced(List<Produccion> woData)
        {
            double totalProduced = 0;
            if (woData != null)
            {
                foreach (Produccion woDataSample in woData)
                {
                    if (MachineState.CheckState(woDataSample.Estado))
                        if (woDataSample.MetrosFabricados != null)
                            totalProduced += (double)woDataSample.MetrosFabricados;
                }
            }
            return totalProduced;
        }

        public static string WoStart(List<Produccion> woData)
        {
            if (woData != null)
                if (woData.Count > 0)
                    return woData[0].Inicio.ToString();
            return "";
        }

        public static string WoWorkingHours(List<Produccion> woData)
        {
            if (woData != null)
                if (woData.Count > 0)
                    return Math.Round((DateTime.Now - woData[0].Inicio).TotalHours, 2).ToString();
            return "";
        }

        public static DateTime ProbableEnd(List<Produccion> woData, ProduccionActual actualData)
        {
            if (woData != null && actualData != null)
            {
                DateTime? woStart = null;
                DateTime? woEnd = actualData.UltimaActualizacion;
                double totalProduced = 0;
                foreach (Produccion woDataSample in woData)
                {
                    if (woStart == null)
                        woStart = woDataSample.Inicio;
                    if (MachineState.CheckState(woDataSample.Estado))
                        if (woDataSample.MetrosFabricados != null)
                            totalProduced += (double)woDataSample.MetrosFabricados;
                }
                if (woStart != null && woEnd != null & actualData.LongitudPieza != null & totalProduced != 0)
                {
                    TimeSpan totalTime = (DateTime)woEnd - (DateTime)woStart;
                    double metersPerSecond = totalProduced / totalTime.TotalSeconds;
                    double totalSecondsNeeded = (int)actualData.LongitudPieza / metersPerSecond;
                    DateTime probableEnd = ((DateTime)woStart).AddSeconds(totalSecondsNeeded);
                    if (DateTime.Now > probableEnd)
                        return DateTime.Now;
                    else
                        return probableEnd;
                }

            }
            return DateTime.Now;
        }

        public static string WoWorkingHoursLeft(DateTime probableEnd)
        {
            double hoursLeft = (probableEnd - DateTime.Now).TotalHours;
            if (hoursLeft > 0)
                return Math.Round(hoursLeft, 2).ToString();
            return "0";
        }

        public static string WoLot(string woIdentifier)
        {
            WO_MSTR wo = WorkOrderDao.Instance.GetByWoId(woIdentifier);
            if (wo != null)
                return wo.wo_lot;
            else
            {
                wo = WorkOrderDao.Instance.GetByWoNbr(woIdentifier);
                if (wo != null)
                    return wo.wo_lot;
            }
            return "";
        }

        public static string WoNbr(string woIdentifier)
        {
            WO_MSTR wo = WorkOrderDao.Instance.GetByWoId(woIdentifier);
            if (wo != null)
                return wo.wo_nbr;
            else
            {
                wo = WorkOrderDao.Instance.GetByWoNbr(woIdentifier);
                if (wo != null)
                    return wo.wo_nbr;
            }
            return "";
        }

        public static string WoParentLot(string woIdentifier)
        {
            return WorkOrderParentLotDao.GetLot(WoLot(woIdentifier));
        }
    }
}
