﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductionControlActualData.DataManage.StopReason
{
    public class WoChangeStopType
    {
        private static List<int?> GetAll
        {
            get
            {
                List<int?> woStopTypes = new List<int?>(); 
                string[] separatedData = Properties.Settings.Default.WoChangeStopTypes.Split(Properties.Settings.Default.WoChangeStopTypesDataSeparator);
                foreach(string stopType in separatedData)
                {
                    int conversionResult = -1;
                    if (Int32.TryParse(stopType, out conversionResult))
                        woStopTypes.Add(conversionResult);
                }
                return woStopTypes;
            }
        }

        public static bool IsWoChange(int idStopType)
        {
            return (GetAll.Find(stopId => stopId == idStopType) != null);
        }
    }
}
