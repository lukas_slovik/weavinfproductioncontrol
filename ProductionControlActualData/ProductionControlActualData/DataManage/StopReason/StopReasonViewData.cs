﻿using ProductionControlActualData.DataAccess;
using ProductionControlActualData.DataManage.ItemData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductionControlActualData.DataManage.StopReason
{
    public class StopReasonViewData
    {
        public List<TiposParo> AllstopTypes { get; set; }
        public TiposParo SelectedStop { get; set; }
        public int SelectedStopId
        {
            get
            {
                if (SelectedStop != null)
                    return SelectedStop.Codigo;
                return -1;
            }
        }
        public bool IsWoChange { get; set; }
        public string WoLot { get; set; }
        public string WoNbr { get; set; }
        public List<ItemDataForView> ItemNumbers { get; set; }
        public ItemDataForView SelectedItemNumber { get; set; }
        public double WoOrderedQty { get; set; }
        public StopReasonViewData(List<TiposParo> allstopTypes)
        {
            AllstopTypes = allstopTypes;
            IsWoChange = false;
            WoLot = null;
            WoNbr = null;
            ItemNumbers = null;
            this.SelectedItemNumber = null;
            this.WoOrderedQty = 0;
        }
    }
}
