﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductionControlActualData.DataManage
{
    public class Model<T> 
    {
        private string columnName;
        private static T DefaultValue;
        private TryParseHandler<T> handler;
        private T _value;

        public T Value
        {
            get { return _value; }
            set { _value = value; }
        }
        private T TryParse(string value)
        {
            if (String.IsNullOrEmpty(value))
                return DefaultValue;
            T result;
            if (this.handler != null)
            {
                if (this.handler(value, out result))
                    return result;
            }
            else
            {
                try
                {
                    return (T)Convert.ChangeType(value, typeof(T));
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    return DefaultValue;
                }
            }
            return DefaultValue;
        }
        public delegate bool TryParseHandler<T>(string value, out T result);
        public Model(string columnName, T defaultValue, TryParseHandler<T> handler)
        {
            this.columnName = columnName;
            this.handler = handler;
            DefaultValue = defaultValue;
            SetValueDt(null);
        }

        public Model(string columnName, T defaultValue)
        {
            this.columnName = columnName;
            DefaultValue = defaultValue;
            this.handler = null;
            SetValueDt(null);
        }

        public void SetValueDt(DataRow producedRow)
        {
            if (producedRow != null)
                if (producedRow.Table.Columns.Contains(this.columnName))
                {
                    if (!DBNull.Value.Equals(producedRow[this.columnName]))
                    {
                        this._value = TryParse(producedRow[this.columnName].ToString().Trim());
                    }
                    else
                        this._value = DefaultValue;
                }
                else
                    this._value = DefaultValue;
            else
                this._value = DefaultValue;
        }
        public void UpdateDt(DataRow producedRow)
        {
            if (producedRow != null)
            {
                if (producedRow.Table.Columns.Contains(this.columnName))
                {
                    producedRow[this.columnName] = this._value;
                }
            }
        }
        public void ColumnDt(DataTable destTable)
        {
            if (destTable != null)
            {
                if (!destTable.Columns.Contains(this.columnName))
                    destTable.Columns.Add(new DataColumn(this.columnName, typeof(T)));
            }
        }
    }
}
