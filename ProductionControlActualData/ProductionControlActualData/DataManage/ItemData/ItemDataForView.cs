﻿using ProductionControlActualData.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductionControlActualData.DataManage.ItemData
{
    public class ItemDataForView
    {
        private Matricula itemData;
        public string ItemNumber
        {
            get
            {
                if (itemData != null)
                    return itemData.Matricula1;
                return "";
            }
        }

        public string ItemDesc
        {
            get
            {
                return ItemDataDao.Instance.ItemDescription(itemData);
            }
        }
        public ItemDataForView(Matricula itemData)
        {
            this.itemData = itemData;
        }
    }
}
