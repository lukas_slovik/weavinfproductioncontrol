﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductionControlActualData.DataManage.WorkOrder
{
    public class WoAlloc
    {
        public Model<string> WoId { get; set; }
        public Model<string> ItemNumber { get; set; }
        public Model<string> LotSerial { get; set; }
        public Model<string> Reference { get; set; }
        public WoAlloc(DataRow woRow)
        {
            WoId = new Model<string>("lad_nbr", "");
            WoId.SetValueDt(woRow);
            ItemNumber = new Model<string>("lad_part", "");
            ItemNumber.SetValueDt(woRow);
            LotSerial = new Model<string>("lad_lot", "");
            LotSerial.SetValueDt(woRow);
            Reference = new Model<string>("lad_ref", "");
            Reference.SetValueDt(woRow);
        }
    }
}
