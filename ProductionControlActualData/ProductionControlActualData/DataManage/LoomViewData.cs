﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace ProductionControlActualData.DataManage
{
    public class LoomViewData
    {
        public int LoomId { get; set; }
        public string Performance { get; set; }
        public string StatusText { get; set; }
        public string StatusColor { get; set; }
        public bool? IsRunning { get; set; }
        public string StopReasonId { get; set; }
        public string StopReasonDesc { get; set; }
        public int Speed { get; set; }
        public int Peaks { get; set; }
        public double LastRunMeters { get; set; }
        public string ItemNumber { get; set; }
        public string ItemDesc { get; set; }
        public bool? HasWo { get; set; }
        public string WoNbr { get; set; }
        public string WoLot { get; set; }
        public double WoMeters { get; set; }
        public double MetersProduced { get; set; }
        public double MetersLeft { get; set; }
        public string WoStart { get; set; }
        public string WoProbableEnd { get; set; }
        public string WoWorkingHours { get; set; }
        public string WoProbableLeftHours { get; set; }
        public string ShiftId { get; set; }
        public string ShiftStartDate { get; set; }
    }
}
