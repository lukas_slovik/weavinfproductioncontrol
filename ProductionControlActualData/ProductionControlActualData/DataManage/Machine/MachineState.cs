﻿using ProductionControlActualData.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductionControlActualData.DataManage.Machine
{
    public class MachineState
    {
        public static bool CheckState(string status)
        {
            if (status != null)
                return (status.Trim().ToUpper() == Properties.Settings.Default.MachineRunningState);
            return false;
        }

        public static string CheckStateTextView(string status)
        {
            if (status != null)
                if (status.Trim().ToUpper() == Properties.Settings.Default.MachineRunningState)
                    return "BEŽÍ";
                else
                    return "ZASTAVENÝ";
            return "ZASTAVENÝ";
        }
    }
}
