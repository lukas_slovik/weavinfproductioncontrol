﻿using ProductionControlActualData.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductionControlActualData.DataManage.Machine
{
    public class MachineStatus
    {
        public bool IsRunning { get; set; }
        public int MachineId { get; set; }
        public string ItemDesc { get; set; }
        public string Beam { get; set; }
        public MachineStatus(ProduccionActual machineData)
        {
            if (machineData != null)
            {
                IsRunning = MachineState.CheckState(machineData.Estado);
                MachineId = machineData.IdTelar;
            }
            else
            {
                IsRunning = false;
                MachineId = -1;
            }
        }
    }
}
