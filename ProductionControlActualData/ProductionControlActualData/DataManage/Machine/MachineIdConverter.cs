﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductionControlActualData.DataManage.Machine
{
    public class MachineIdConverter
    {
        public static int Convert(string machineId)
        {
            int result = -1;
            Int32.TryParse(Properties.Settings.Default.InitialMachineId, out result);
            if (machineId != null)
            {
                int convertResult = 0;
                Int32.TryParse(machineId, out convertResult);
                if (convertResult != 0)
                    result = convertResult;
            }
            return result;
        }
    }
}
