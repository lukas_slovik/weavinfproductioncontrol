﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProductionControlActualData.View.Machine
{
    /// <summary>
    /// Interaction logic for MachineView.xaml
    /// </summary>
    public partial class MachineView : UserControl
    {
        public MachineView()
        {
            InitializeComponent();
        }
        public string Loom
        {
            get
            {
                return this.loom.Content.ToString();
            }
            set
            {
                this.loom.Content = value;
            }
        }
        public string Beam
        {
            get
            {
                return this.beam.Content.ToString();
            }
            set
            {
                this.beam.Content = value;
            }
        }
        public string ItemDesc
        {
            get
            {
                return this.itemDesc.Content.ToString();
            }
            set
            {
                this.itemDesc.Content = value;
            }
        }

        public Brush LoomColor
        {
            get
            {
                return this.loom.Foreground;
            }
            set
            {
                this.loom.Foreground = value;
            }
        }
    }
}
