﻿using ProductionControlActualData.DataManage.StopReason;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductionControlActualData.View
{
    public interface ChangeStopViewMode
    {
        void InitializeView(StopReasonViewData viewData);
        void CloseView();
    }
}
