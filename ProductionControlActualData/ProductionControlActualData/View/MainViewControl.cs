﻿using ProductionControlActualData.DataManage;
using ProductionControlActualData.DataManage.Machine;
using ProductionControlActualData.MVC.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductionControlActualData.View
{
    public interface MainViewControl
    {
        void MachineStatusUpdated(List<MachineStatus> machines);
        void InitializeView(LoomViewData viewData);
        void CloseView();
        void ShowChangeStopView(StopChangeController controller);
        void ShowShiftChangeView(ShiftChangeController controller);
        void ReturnToMainView();
    }
}
