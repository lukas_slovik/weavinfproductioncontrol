﻿using ProductionControlActualData.DataManage;
using ProductionControlActualData.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ProductionControlActualData.DataManage.Machine;
using ProductionControlActualData.MVC.Controller;
using System.Reflection;
using ProductionControlActualData.View.ShiftChange;
using ProductionControlActualData.View.Machine;

namespace ProductionControlActualData.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, MainViewControl
    {
        private List<MachineView> machineBtns;
        private MainControllerView control;
        private void SetBtns()
        {
            this.machineBtns = new List<MachineView>();
            this.machineBtns.Add(this.loom105.Content as MachineView);
            this.machineBtns.Add(this.loom106.Content as MachineView);
            this.machineBtns.Add(this.loom103.Content as MachineView);
            this.machineBtns.Add(this.loom104.Content as MachineView);
            this.machineBtns.Add(this.loom101.Content as MachineView);
            this.machineBtns.Add(this.loom102.Content as MachineView);
            this.machineBtns.Add(this.loom119.Content as MachineView);
            this.machineBtns.Add(this.loom120.Content as MachineView);
            this.machineBtns.Add(this.loom121.Content as MachineView);
            this.machineBtns.Add(this.loom122.Content as MachineView);
            this.machineBtns.Add(this.loom123.Content as MachineView);
            this.machineBtns.Add(this.loom124.Content as MachineView);
            this.machineBtns.Add(this.loom113.Content as MachineView);
            this.machineBtns.Add(this.loom114.Content as MachineView);
            this.machineBtns.Add(this.loom115.Content as MachineView);
            this.machineBtns.Add(this.loom116.Content as MachineView);
            this.machineBtns.Add(this.loom117.Content as MachineView);
            this.machineBtns.Add(this.loom118.Content as MachineView);
            this.machineBtns.Add(this.loom131.Content as MachineView);
            this.machineBtns.Add(this.loom132.Content as MachineView);
            this.machineBtns.Add(this.loom133.Content as MachineView);
            this.machineBtns.Add(this.loom134.Content as MachineView);
            this.machineBtns.Add(this.loom135.Content as MachineView);
            this.machineBtns.Add(this.loom136.Content as MachineView);
            this.machineBtns.Add(this.loom125.Content as MachineView);
            this.machineBtns.Add(this.loom126.Content as MachineView);
            this.machineBtns.Add(this.loom127.Content as MachineView);
            this.machineBtns.Add(this.loom128.Content as MachineView);
            this.machineBtns.Add(this.loom129.Content as MachineView);
            this.machineBtns.Add(this.loom130.Content as MachineView);
            this.machineBtns.Add(this.loom143.Content as MachineView);
            this.machineBtns.Add(this.loom144.Content as MachineView);
            this.machineBtns.Add(this.loom146.Content as MachineView);
            this.machineBtns.Add(this.loom145.Content as MachineView);
            this.machineBtns.Add(this.loom148.Content as MachineView);
            this.machineBtns.Add(this.loom137.Content as MachineView);
            this.machineBtns.Add(this.loom138.Content as MachineView);
            this.machineBtns.Add(this.loom139.Content as MachineView);
            this.machineBtns.Add(this.loom140.Content as MachineView);
            this.machineBtns.Add(this.loom141.Content as MachineView);
            this.machineBtns.Add(this.loom142.Content as MachineView);
            this.machineBtns.Add(this.loom111.Content as MachineView);
            this.machineBtns.Add(this.loom112.Content as MachineView);
            this.machineBtns.Add(this.loom109.Content as MachineView);
            this.machineBtns.Add(this.loom110.Content as MachineView);
            this.machineBtns.Add(this.loom107.Content as MachineView);
            this.machineBtns.Add(this.loom108.Content as MachineView);
        }

        public MainWindow()
        {
            this.control = new MainController();
            InitializeComponent();
            SetBtns();
        }

        private void UpdateMachine(MachineView loom, List<MachineStatus> machines)
        {
            SolidColorBrush newBrush = (SolidColorBrush)(new BrushConverter().ConvertFrom(Properties.Settings.Default.StoppedStatusColor));
            if (machines != null)
            {
                MachineStatus loomStatus = machines.Find(loomData => loomData.MachineId.ToString() == loom.Loom.ToString());
                if (loomStatus != null)
                {
                    if (loomStatus.IsRunning)
                        newBrush = (SolidColorBrush)(new BrushConverter().ConvertFrom(Properties.Settings.Default.RunningStatusColor));
                    loom.Beam = loomStatus.Beam;
                    loom.ItemDesc = loomStatus.ItemDesc;
                }
            }
            loom.LoomColor = newBrush;
        }
        #region view events
        private void mainView_Loaded(object sender, RoutedEventArgs e)
        {
            InitializeView(null);
            this.control.ViewLoaded(this);
        }

        private void mainView_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.control.CloseView();
        }

        private void loomsE_Expanded(object sender, RoutedEventArgs e)
        {
            this.control.MachinesVisible = true;
        }

        private void loomsE_Collapsed(object sender, RoutedEventArgs e)
        {
            this.control.MachinesVisible = false;
        }

        private void LoomBtn_Click(object sender, RoutedEventArgs e)
        {
            this.loomsE.IsExpanded = false;
            this.control.MachineIdChanged((((Button)sender).Content as MachineView).Loom.ToString());
        }

        private void changeStopStatusBtn_Click(object sender, RoutedEventArgs e)
        {
            this.control.ChangeStopReason();
        }

        private void shiftChangeBtn_Click(object sender, RoutedEventArgs e)
        {
            this.control.ChangeShift();
        }
        #endregion
        #region MainViewControl interface implementation
        public void MachineStatusUpdated(List<MachineStatus> machines)
        {
            foreach (MachineView loom in this.machineBtns)
            {
                Dispatcher.BeginInvoke((Action<MachineView, List<MachineStatus>>)(UpdateMachine), new object[] { loom, machines });
            }
        }
        public void InitializeView(LoomViewData viewData)
        {
            if (viewData != null)
            {
                Dispatcher.BeginInvoke((Action)(() => loomIdTb.Text = viewData.LoomId.ToString()));
                Dispatcher.BeginInvoke((Action)(() => performanceTb.Text = viewData.Performance));
                Dispatcher.BeginInvoke((Action)(() => statusTb.Text = viewData.StatusText));
                Dispatcher.BeginInvoke((Action)(() => statusTb.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom(viewData.StatusColor))));
                //if (viewData.IsRunning != null)
                //{
                //    if ((bool)viewData.IsRunning)
                //        Dispatcher.BeginInvoke((Action)(() => stopReasonG.Visibility = Visibility.Visible));
                //    else
                //        Dispatcher.BeginInvoke((Action)(() => stopReasonG.Visibility = Visibility.Hidden));
                //}
                //else
                //    Dispatcher.BeginInvoke((Action)(() => stopReasonG.Visibility = Visibility.Hidden));
                Dispatcher.BeginInvoke((Action)(() => stopStatusIdTb.Text = viewData.StopReasonId));
                Dispatcher.BeginInvoke((Action)(() => stopStatusDescTb.Text = viewData.StopReasonDesc));
                Dispatcher.BeginInvoke((Action)(() => speedTb.Text = viewData.Speed.ToString()));
                Dispatcher.BeginInvoke((Action)(() => peaksTb.Text = viewData.Peaks.ToString()));
                Dispatcher.BeginInvoke((Action)(() => prodMeterTb.Text = Math.Round(viewData.MetersProduced,2).ToString()));
                Dispatcher.BeginInvoke((Action)(() => itemNumberTb.Text = viewData.ItemNumber));
                Dispatcher.BeginInvoke((Action)(() => itemNumberDescLb.Content = viewData.ItemDesc));
                Dispatcher.BeginInvoke((Action)(() => woNbrTb.Text = viewData.WoNbr));
                Dispatcher.BeginInvoke((Action)(() => woLotTb.Text = viewData.WoLot));
                Dispatcher.BeginInvoke((Action)(() => woMetersTb.Text = Math.Round(viewData.WoMeters, 2).ToString()));
                Dispatcher.BeginInvoke((Action)(() => producedMetersTb.Text = Math.Round(viewData.MetersProduced, 2).ToString()));
                Dispatcher.BeginInvoke((Action)(() => metersLeftTb.Text = Math.Round(viewData.MetersLeft, 2).ToString()));
                Dispatcher.BeginInvoke((Action)(() => startTimeTb.Text = viewData.WoStart));
                Dispatcher.BeginInvoke((Action)(() => timeRunningTb.Text = viewData.WoWorkingHours));
                Dispatcher.BeginInvoke((Action)(() => stopTimeTb.Text = viewData.WoProbableEnd));
                Dispatcher.BeginInvoke((Action)(() => timeLeftTb.Text = viewData.WoProbableLeftHours));
                Dispatcher.BeginInvoke((Action)(() => shiftIdTb.Text = viewData.ShiftId));
                Dispatcher.BeginInvoke((Action)(() => shiftStartDateTimeTb.Text = viewData.ShiftStartDate));
            }
            else
            {
                Dispatcher.BeginInvoke((Action)(() => loomIdTb.Text = ""));
                Dispatcher.BeginInvoke((Action)(() => performanceTb.Text = ""));
                Dispatcher.BeginInvoke((Action)(() => statusTb.Text = ""));
                Dispatcher.BeginInvoke((Action)(() => statusTb.Background = Brushes.White));
                Dispatcher.BeginInvoke((Action)(() => stopStatusIdTb.Text = ""));
                Dispatcher.BeginInvoke((Action)(() => stopStatusDescTb.Text = ""));
                Dispatcher.BeginInvoke((Action)(() => speedTb.Text = ""));
                Dispatcher.BeginInvoke((Action)(() => peaksTb.Text = ""));
                Dispatcher.BeginInvoke((Action)(() => prodMeterTb.Text = ""));
                Dispatcher.BeginInvoke((Action)(() => itemNumberTb.Text = ""));
                Dispatcher.BeginInvoke((Action)(() => itemNumberDescLb.Content = ""));
                Dispatcher.BeginInvoke((Action)(() => woNbrTb.Text = ""));
                Dispatcher.BeginInvoke((Action)(() => woLotTb.Text = ""));
                Dispatcher.BeginInvoke((Action)(() => woMetersTb.Text = ""));
                Dispatcher.BeginInvoke((Action)(() => producedMetersTb.Text = ""));
                Dispatcher.BeginInvoke((Action)(() => metersLeftTb.Text = ""));
                Dispatcher.BeginInvoke((Action)(() => startTimeTb.Text = ""));
                Dispatcher.BeginInvoke((Action)(() => timeRunningTb.Text = ""));
                Dispatcher.BeginInvoke((Action)(() => stopTimeTb.Text = ""));
                Dispatcher.BeginInvoke((Action)(() => timeLeftTb.Text = ""));
                Dispatcher.BeginInvoke((Action)(() => shiftIdTb.Text = ""));
                Dispatcher.BeginInvoke((Action)(() => shiftStartDateTimeTb.Text = ""));
            }
        }

        public void CloseView()
        {
            Dispatcher.BeginInvoke((Action)(() => this.Close()));
        }

        public void ShowChangeStopView(StopChangeController controller)
        {
            Dispatcher.BeginInvoke((Action)(() => userControlView.Content = new ChangeStopView(controller)));
            Dispatcher.BeginInvoke((Action)(() => changeView.SelectedIndex = 1));
        }

        public void ShowShiftChangeView(ShiftChangeController controller)
        {
            Dispatcher.BeginInvoke((Action)(() => userControlView.Content = new ShiftChangeView(controller)));
            Dispatcher.BeginInvoke((Action)(() => changeView.SelectedIndex = 1));
        }

        public void ReturnToMainView()
        {
            Dispatcher.BeginInvoke((Action)(() => changeView.SelectedIndex = 0));
        }
        #endregion
    }
}
