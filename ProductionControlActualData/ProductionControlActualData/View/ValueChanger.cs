﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProductionControlActualData.View
{
    public class ValueChanger
    {
        public bool StopChanger { get; set; }
        public static bool StopChangers { get; set; }
        public delegate void ValueChanged(object resultValue);
        private ValueChanged Result { get; set; }
        private DateTime? changeStart;
        private object newValue;
        private Thread checkingThread;
        public void ValueChange(object newValue)
        {
            this.newValue = newValue;
            this.changeStart = DateTime.Now;
            if (checkingThread == null)
            {
                this.checkingThread = new Thread(WaitForDataChange);
                this.checkingThread.Start();
            }
                
        }

        private void WaitForDataChange()
        {
            while (!StopChangers && !StopChanger)
            {
                if ((DateTime.Now - (DateTime)this.changeStart).TotalMilliseconds >= Properties.Settings.Default.DataCheckUserEnter)
                {
                    if (Result != null)
                        Result(this.newValue);
                    break;
                }
                Thread.Sleep(Properties.Settings.Default.EndlessLoopTimeOut);
            }
            StopChanger = true;
        } 

        public ValueChanger(ValueChanged changer)
        {
            StopChangers = false;
            StopChanger = false;
            changeStart = null;
            checkingThread = null;
            Result = changer;
        }
    }
}
