﻿using ProductionControlActualData.MVC.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ProductionControlActualData.DataManage.StopReason;
using ProductionControlActualData.DataAccess;
using ProductionControlActualData.DataManage.ItemData;

namespace ProductionControlActualData.View
{
    /// <summary>
    /// Interaction logic for ChangeStopView.xaml
    /// </summary>
    public partial class ChangeStopView : UserControl, ChangeStopViewMode
    {
        private StopChangeController controller;
        private StopReasonViewData viewData;
        public ChangeStopView(StopChangeController controller)
        {
            this.controller = controller;
            this.viewData = null;
            InitializeComponent();
        }
        public ChangeStopView()
        {
            this.controller = null;
            this.viewData = null;
            InitializeComponent();
        }
        #region view events
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (controller != null)
                controller.ChangeStopViewLoaded(this);
        }

        private void cancelBtn_Click(object sender, RoutedEventArgs e)
        {
            if (controller != null)
                controller.ViewClosed();
            this.Visibility = Visibility.Collapsed;
        }

        private void okBtn_Click(object sender, RoutedEventArgs e)
        {
            if (controller != null)
                controller.StopReasonDataComfirmed(this.viewData);
        }

        private void stopReasonCb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.stopReasonCb.IsDropDownOpen)
            {
                if (this.viewData != null)
                {
                    this.viewData.SelectedStop = this.stopReasonCb.SelectedItem as TiposParo;
                    this.controller.StopReasonChanged(this.viewData);
                }
            }
        }

        private void workOrderIdTb_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox woId = sender as TextBox;
            if (woId != null)
            {
                this.viewData.WoLot = woId.Text;
                this.controller.WorkOrderIdChanged(this.viewData);
            }
        }

        private void workOrderNbrTb_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox woNbr = sender as TextBox;
            if (woNbr != null)
            {
                this.viewData.WoNbr = woNbr.Text;
                this.controller.WorkOrderIdChanged(this.viewData);
            }
        }

        private void itemNumberCb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.itemNumberCb.IsDropDownOpen)
                if (this.viewData != null)
                    this.viewData.SelectedItemNumber = this.itemNumberCb.SelectedItem as ItemDataForView;
        }
        #endregion
        #region ChangeStopViewMode interface implementation
        public void InitializeView(StopReasonViewData viewData)
        {
            this.viewData = viewData;
            if (viewData != null)
            {
                Dispatcher.BeginInvoke((Action)(() => this.stopReasonCb.ItemsSource = viewData.AllstopTypes));
                Dispatcher.BeginInvoke((Action)(() => this.stopReasonCb.SelectedValuePath = "Codigo"));
                Dispatcher.BeginInvoke((Action)(() => this.stopReasonCb.DisplayMemberPath = "Descripcion"));
                Dispatcher.BeginInvoke((Action)(() => this.stopReasonCb.SelectedItem = viewData.SelectedStop));
                if (viewData.IsWoChange)
                    Dispatcher.BeginInvoke((Action)(() => this.woGrid.Visibility = Visibility.Visible));
                else
                    Dispatcher.BeginInvoke((Action)(() => this.woGrid.Visibility = Visibility.Hidden));
                Dispatcher.BeginInvoke((Action)(() => this.workOrderIdTb.Text = viewData.WoLot));
                Dispatcher.BeginInvoke((Action)(() => this.workOrderNbrTb.Text = viewData.WoNbr));
                Dispatcher.BeginInvoke((Action)(() => this.itemNumberCb.ItemsSource = viewData.ItemNumbers));
                Dispatcher.BeginInvoke((Action)(() => this.itemNumberCb.SelectedValuePath = "ItemNumber"));
                Dispatcher.BeginInvoke((Action)(() => this.itemNumberCb.DisplayMemberPath = "ItemDesc"));
                Dispatcher.BeginInvoke((Action)(() => this.itemNumberCb.SelectedItem = viewData.SelectedItemNumber));
                Dispatcher.BeginInvoke((Action)(() => this.qtyOrderedTb.Text = viewData.WoOrderedQty.ToString()));
            }
            else
            {
                if (controller != null)
                    controller.ViewClosed();
                Dispatcher.BeginInvoke((Action)(() => this.Visibility = Visibility.Collapsed));
            }

        }

        public void CloseView()
        {
            Dispatcher.BeginInvoke((Action)(() => this.Visibility = Visibility.Collapsed));
        }
        #endregion
    }
}
