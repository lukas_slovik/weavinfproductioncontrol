﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ProductionControlActualData.DataAccess;
using ProductionControlActualData.MVC.Controller;

namespace ProductionControlActualData.View.ShiftChange
{
    /// <summary>
    /// Interaction logic for ShiftChangeView.xaml
    /// </summary>
    public partial class ShiftChangeView : UserControl, ShiftChangeViewMode
    {
        private ShiftChangeController controller;
        public ShiftChangeView(ShiftChangeController controller)
        {
            this.controller = controller;
            InitializeComponent();
        }
        #region userControl view events
        private void shiftCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void okBtn_Click(object sender, RoutedEventArgs e)
        {
            if (this.shiftCB.SelectedItem != null)
                this.controller.ShiftSelected(this.shiftCB.SelectedItem as equipos);
        }

        private void cancelBtn_Click(object sender, RoutedEventArgs e)
        {
            this.controller.CloseShiftView();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            InitializeView(null);
            this.controller.ShiftViewLoaded(this);
        }
        #endregion
        #region
        public void InitializeView(List<equipos> shiftData)
        {
            if (shiftData != null)
            {
                Dispatcher.BeginInvoke((Action)(() => this.shiftCB.ItemsSource = shiftData));
                Dispatcher.BeginInvoke((Action)(() => this.shiftCB.SelectedValuePath = "Equipo"));
                Dispatcher.BeginInvoke((Action)(() => this.shiftCB.DisplayMemberPath = "NombreEquipo"));
                Dispatcher.BeginInvoke((Action)(() => this.okBtn.Visibility = Visibility.Visible));
            }
            else
            {
                Dispatcher.BeginInvoke((Action)(() => this.okBtn.Visibility = Visibility.Collapsed));
            }
        }

        public void CloseView()
        {
            Dispatcher.BeginInvoke((Action)(() => this.Visibility = Visibility.Collapsed));
        }
        #endregion
    }
}
