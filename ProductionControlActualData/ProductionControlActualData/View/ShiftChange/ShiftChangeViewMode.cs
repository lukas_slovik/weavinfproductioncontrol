﻿using ProductionControlActualData.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductionControlActualData.View.ShiftChange
{
    public interface ShiftChangeViewMode
    {
        void InitializeView(List<equipos> shiftData);
        void CloseView();
    }
}
