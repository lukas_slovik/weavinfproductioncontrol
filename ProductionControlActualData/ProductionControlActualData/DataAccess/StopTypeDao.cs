﻿using ProductionControlActualData.InfoHandle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductionControlActualData.DataAccess
{
    public class StopTypeDao : TimeRelevant
    {
        private static StopTypeDao instance;

        public static StopTypeDao Instance
        {
            get
            {
                if (instance == null)
                    instance = new StopTypeDao();
                return instance;
            }
        }

        protected override int TimeOutPeriod
        {
            get
            {
                return Properties.Settings.Default.StopTypeRefreshTime;
            }
        }

        protected override bool NotInitialized
        {
            get
            {
                return (this.allStops == null);
            }
        }

        private List<TiposParo> allStops;
        private object allStopsLock;
        private StopTypeDao()
        {
            this.allStopsLock = new object();
            this.allStops = null;
        }

        public List<TiposParo> AllStops
        {
            get
            {
                if (ShouldRefresh)
                {
                    using (var Context = new CPTelaresEntities())
                    {
                        var requestQuery = from stop in Context.TiposParos
                                           select stop;
                        try
                        {
                            lock (this.allStopsLock)
                            {
                                this.allStops = requestQuery.ToList<TiposParo>();
                            }
                            Updated(DateTime.Now);
                        }
                        catch (Exception e)
                        {
                            EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                        }
                    }
                }
                lock (this.allStopsLock)
                {
                    if (this.allStops != null)
                        return new List<TiposParo>(this.allStops);
                    return new List<TiposParo>();
                }
            }
        }

        public TiposParo StopById(int? stopId)
        {
            if (stopId != null)
                return AllStops.Find(stop => stop.Codigo == stopId);
            return null;
        }

        public string StopDescriptionById(int? stopId)
        {
            if (stopId != null)
            {
                TiposParo foundStop = AllStops.Find(stop => stop.Codigo == stopId);
                if (foundStop != null)
                    return foundStop.Descripcion;
            }
            return "";
        }
    }
}
