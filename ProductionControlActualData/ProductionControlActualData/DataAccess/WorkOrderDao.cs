﻿using ProductionControlActualData.InfoHandle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductionControlActualData.DataAccess
{
    public class WorkOrderDao
    {
        private static WorkOrderDao instance;

        public static WorkOrderDao Instance
        {
            get
            {
                if (instance == null)
                    instance = new WorkOrderDao();
                return instance;
            }
        }

        private WorkOrderDao()
        {

        }

        public WO_MSTR GetByWoNbr(string woNbr)
        {
            if (woNbr != null)
            {
                using (var Context = new MFGPRO_SQLEntities())
                {
                    var requestQuery = from wokOrder in Context.WO_MSTR
                                       where wokOrder.wo_nbr == woNbr
                                       select wokOrder;
                    try
                    {
                        List<WO_MSTR> workOrders = requestQuery.ToList<WO_MSTR>();
                        if (workOrders.Count == 1)
                            return workOrders[0];
                    }
                    catch (Exception e)
                    {
                        EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                    }
                }
            }
            return null;
        }

        public WO_MSTR GetByWoId(string woLot)
        {
            if (woLot != null)
            {
                using (var Context = new MFGPRO_SQLEntities())
                {
                    var requestQuery = from wokOrder in Context.WO_MSTR
                                       where wokOrder.wo_lot == woLot
                                       select wokOrder;
                    try
                    {
                        List<WO_MSTR> workOrders = requestQuery.ToList<WO_MSTR>();
                        if (workOrders.Count == 1)
                            return workOrders[0];
                    }
                    catch (Exception e)
                    {
                        EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                    }
                }
            }
            return null;
        }

        public List<WOD_DET> WorkOrderParts(string woLot)
        {
            if (woLot != null)
            {
                using (var Context = new MFGPRO_SQLEntities())
                {
                    var requestQuery = from wokOrder in Context.WOD_DET
                                       where wokOrder.wod_lot == woLot
                                       select wokOrder;
                    try
                    {
                        return requestQuery.ToList<WOD_DET>();
                    }
                    catch (Exception e)
                    {
                        EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                    }
                }
            }
            return new List<WOD_DET>();
        }
    }
}
