﻿using ProductionControlActualData.InfoHandle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductionControlActualData.DataAccess
{
    public class WorkOrderMachineDataDao
    {
        private static WorkOrderMachineDataDao instance;

        public static WorkOrderMachineDataDao Instance
        {
            get
            {
                if (instance == null)
                    instance = new WorkOrderMachineDataDao();
                return instance;
            }
        }

        private WorkOrderMachineDataDao()
        {

        }

        public List<Produccion> ByWoId(string woId)
        {
            if (woId == null)
                return new List<Produccion>();
            using (var Context = new CPTelaresEntities())
            {
                var requestQuery = from woData in Context.Produccions
                                   where woData.OrdenProduccion == woId
                                   orderby woData.Inicio
                                   select woData;
                try
                {
                    return requestQuery.ToList<Produccion>();
                }
                catch (Exception e)
                {
                    EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                }
            }
            return new List<Produccion>();
        }

        public void WorkOrderEntered(string noWoLot, string newWoLot, string itemNumber, double plannedQty)
        {
            if (noWoLot != null && newWoLot != null && itemNumber != null)
            {
                using (var Context = new CPTelaresEntities())
                {
                    var requestQuery = from woData in Context.Produccions
                                       where woData.OrdenProduccion == noWoLot
                                       select woData;
                    try
                    {
                        List<Produccion> allWoData = requestQuery.ToList<Produccion>();
                        foreach (Produccion woSample in allWoData)
                        {
                            woSample.OrdenProduccion = newWoLot;
                            woSample.Articulo = itemNumber.Trim().ToUpper();
                            woSample.LongitudPieza = (int)plannedQty;
                        }
                        Context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                    }
                }
            }
        }


    }
}
