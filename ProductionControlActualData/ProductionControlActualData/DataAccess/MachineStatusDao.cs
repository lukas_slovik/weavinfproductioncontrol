﻿using ProductionControlActualData.DataManage;
using ProductionControlActualData.DataManage.Machine;
using ProductionControlActualData.InfoHandle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductionControlActualData.DataAccess
{
    public class MachineStatusDao
    {
        private static MachineStatusDao instance;

        public static MachineStatusDao Instance
        {
            get
            {
                if (instance == null)
                    instance = new MachineStatusDao();
                return instance;
            }
        }

        private MachineStatusDao()
        {

        }

        public List<ProduccionActual> GetStatus()
        {
            using (var Context = new CPTelaresEntities())
            {
                var requestQuery = from machineData in Context.ProduccionActuals
                                   select machineData;
                try
                {
                    return requestQuery.ToList<ProduccionActual>();
                }
                catch (Exception e)
                {
                    EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                }
            }
            return null;
        }

        public List<MachineStatus> GetMachineStatus()
        {
            List<MachineStatus> machineState = new List<MachineStatus>();
            List<ProduccionActual> machineData = GetStatus();
            if (machineData != null)
            {
                foreach (ProduccionActual machine in machineData)
                {
                    MachineStatus machineStat = new MachineStatus(machine);
                    machineStat.ItemDesc = ItemDataDao.Instance.ItemDescription(machine.Articulo);
                    machineStat.Beam = machine.Pieza.Trim();
                    machineState.Add(machineStat);
                }
            }
            return machineState;
        }
    }
}
