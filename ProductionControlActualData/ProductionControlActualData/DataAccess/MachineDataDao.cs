﻿using ProductionControlActualData.InfoHandle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductionControlActualData.DataAccess
{
    public class MachineDataDao
    {
        private static MachineDataDao instance;

        public static MachineDataDao Instance
        {
            get
            {
                if (instance == null)
                    instance = new MachineDataDao();
                return instance;
            }
        }

        private MachineDataDao()
        {

        }

        public ProduccionActual GetMachineData(int machineId)
        {
            using (var Context = new CPTelaresEntities())
            {
                var requestQuery = from machineData in Context.ProduccionActuals
                                   where machineData.IdTelar == machineId
                                   select machineData;
                try
                {
                    return requestQuery.FirstOrDefault<ProduccionActual>();
                }
                catch (Exception e)
                {
                    EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                }
            }
            return null;
        }

        public string GetCurrentWo(int machineId)
        {
            ProduccionActual machineData = GetMachineData(machineId);
            if (machineData != null)
            {
                return machineData.OrdenProduccion;
            }
            return null;
        }

        public bool IsNoWoState(int machineId)
        {
            ProduccionActual machineData = GetMachineData(machineId);
            if (machineData != null)
            {
                if (machineData.OrdenProduccion == null)
                    return true;
                if (machineData.OrdenProduccion.Trim() == "")
                    return true;
                if (machineData.OrdenProduccion.IndexOf(Properties.Settings.Default.NoWorkOrderIndentificator) > -1)
                    return true;
                return false;
            }
            return true;
        }

        public void UpdateMachineStopReason(int machineId,int stopReason)
        {
            using (var Context = new CPTelaresEntities())
            {
                var requestQuery = from machineData in Context.ProduccionActuals
                                   where machineData.IdTelar == machineId
                                   select machineData;
                try
                {
                    ProduccionActual machineData= requestQuery.FirstOrDefault<ProduccionActual>();
                    if (machineData != null)
                        machineData.CodigoParo = stopReason;
                    Context.SaveChanges();
                }
                catch (Exception e)
                {
                    EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                }
            }
        }

        public void AddNewWO(int machineId, string woLot, string itemNumber,string beamLot, double plannedQty, int stopReasonId)
        {
            if (woLot != null && itemNumber != null)
            {
                if (woLot.Trim().Length > 0 && itemNumber.Trim().Length > 0)
                {
                    using (var Context = new CPTelaresEntities())
                    {
                        var requestQuery = from machineData in Context.ProduccionActuals
                                           where machineData.IdTelar == machineId
                                           select machineData;
                        try
                        {
                            ProduccionActual machineData = requestQuery.FirstOrDefault<ProduccionActual>();
                            if (machineData != null)
                            {
                                machineData.Articulo = itemNumber.Trim().ToUpper();
                                machineData.OrdenProduccion = woLot;
                                machineData.LongitudPieza = (int)plannedQty;
                                machineData.CodigoParo = stopReasonId;
                                machineData.Pieza = beamLot;
                                Context.SaveChanges();
                            }
                        }
                        catch (Exception e)
                        {
                            EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                        }
                    }
                }
            }
        }

        public void ChangeShift(equipos selectedShift)
        {
            if (selectedShift != null)
            {
                using (var Context = new CPTelaresEntities())
                {
                    var requestQuery = from machineData in Context.ProduccionActuals
                                       select machineData;
                    try
                    {
                        List<ProduccionActual> machineData = requestQuery.ToList<ProduccionActual>();
                        DateTime shiftChangeDate= DateTime.Now;
                        foreach (ProduccionActual machine in machineData)
                        {
                            machine.Turno = selectedShift.Equipo;
                            machine.DiaTurno = shiftChangeDate;
                        }
                        Context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                    }
                }
            }
        }
    }
}
