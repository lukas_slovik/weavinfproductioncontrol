﻿using ProductionControlActualData.InfoHandle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductionControlActualData.DataAccess
{
    public class ShiftDao : TimeRelevant
    {
        private static ShiftDao instance;

        public static ShiftDao Instance
        {
            get
            {
                if (instance == null)
                    instance = new ShiftDao();
                return instance;
            }
        }

        protected override int TimeOutPeriod
        {
            get
            {
                return Properties.Settings.Default.ShiftChangeRefreshTime;
            }
        }

        protected override bool NotInitialized
        {
            get
            {
                return (this.allShifts == null);
            }
        }

        private List<equipos> allShifts;
        private object allShiftsLock;
        private ShiftDao()
        {
            this.allShifts = null;
            this.allShiftsLock = new object();
        }

        public List<equipos> AllShifts
        {
            get
            {
                if (ShouldRefresh)
                {
                    using (var Context = new CPTelaresEntities())
                    {
                        var requestQuery = from shiftData in Context.equipos
                                           select shiftData;
                        try
                        {
                            lock (this.allShiftsLock)
                            {
                                this.allShifts = requestQuery.ToList<equipos>();
                            }
                            Updated(DateTime.Now);
                        }
                        catch (Exception e)
                        {
                            EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                        }
                    }
                }
                lock (this.allShiftsLock)
                {
                    if (this.allShifts != null)
                        return new List<equipos>(this.allShifts);
                    return new List<equipos>();
                }
            }
        }

        public equipos GetById(string shiftId)
        {
            if (shiftId != null)
            {
                List<equipos> shiftsData = AllShifts;
                return shiftsData.Find(shift => shift.Equipo.Trim() == shiftId.Trim());
            }
            return null;
        }

        public void ChangeShift(equipos changedShift)
        {
            if (changedShift != null)
            {
                using (var Context = new CPTelaresEntities())
                { 
                    try
                    {
                        CambioTurno shiftChange = new CambioTurno();
                        shiftChange.Equipo = changedShift.Equipo;
                        shiftChange.FechaHora = DateTime.Now;
                        Context.CambioTurnoes.Add(shiftChange);
                        Context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                    }
                }
            }
        }
    }
}
