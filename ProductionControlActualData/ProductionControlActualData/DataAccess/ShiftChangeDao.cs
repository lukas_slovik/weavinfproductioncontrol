﻿using ProductionControlActualData.InfoHandle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductionControlActualData.DataAccess
{
    public class ShiftChangeDao
    {
        private static ShiftChangeDao instance;

        public static ShiftChangeDao Instance
        {
            get
            {
                if (instance == null)
                    instance = new ShiftChangeDao();
                return instance;
            }
        }

        private ShiftChangeDao()
        {

        }

        public CambioTurno LastShift()
        {
            using (var Context = new CPTelaresEntities())
            {
                var requestQuery = from woData in Context.CambioTurnoes
                                   orderby woData.FechaHora descending
                                   select woData;
                try
                {
                    return requestQuery.FirstOrDefault<CambioTurno>();
                }
                catch (Exception e)
                {
                    EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                }
            }
            return null;
        }
    }
}
