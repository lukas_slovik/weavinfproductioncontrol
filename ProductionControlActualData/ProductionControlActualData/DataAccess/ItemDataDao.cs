﻿using ProductionControlActualData.InfoHandle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductionControlActualData.DataAccess
{
    public class ItemDataDao : TimeRelevant
    {
        private static ItemDataDao instance;

        public static ItemDataDao Instance
        {
            get
            {
                if (instance == null)
                    instance = new ItemDataDao();
                return instance;
            }
        }
        private object itemsDataLock;
        private List<Matricula> itemsData;
        private ItemDataDao()
        {
            this.itemsData = null;
            this.itemsDataLock = new object();
        }

        public List<Matricula> ItemsData
        {
            get
            {
                if (ShouldRefresh)
                {
                    using (var Context = new MFGPRO_SQLEntities())
                    {
                        var requestQuery = from items in Context.Matriculas
                                           where items.PT_STATUS.Trim().ToUpper() != Properties.Settings.Default.ItemObsoleteStatus
                                           select items;
                        try
                        {
                            lock (this.itemsDataLock)
                            {
                                this.itemsData = requestQuery.ToList<Matricula>();
                            }
                            Updated(DateTime.Now);
                        }
                        catch (Exception e)
                        {
                            EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                        }
                    }
                }
                lock (this.itemsDataLock)
                {
                    if (this.itemsData != null)
                        return new List<Matricula>(this.itemsData);
                    return new List<Matricula>();
                }
            }
        }

        public List<Matricula> ItemsForGroup
        {
            get
            {
                List<Matricula> allItems = ItemsData;
                List<Matricula> groupItems = new List<Matricula>();
                foreach (Matricula item in allItems)
                {
                    if (item.TipoArticulo.Trim().ToUpper() == Properties.Settings.Default.ItemType.Trim().ToUpper())
                        groupItems.Add(item);
                }
                return groupItems;
            }
        }

        protected override int TimeOutPeriod
        {
            get
            {
                return Properties.Settings.Default.ItemDataRefreshTime;
            }
        }

        protected override bool NotInitialized
        {
            get
            {
                if (this.itemsData == null)
                    return true;
                return false;
            }
        }

        public Matricula GetItemData(string itemNumber)
        {
            List<Matricula> currnetItems = ItemsData;
            if (currnetItems != null)
                return currnetItems.Find(item => item.Matricula1.Trim().ToUpper() == itemNumber.Trim().ToUpper());
            return null;
        }

        public string ItemDescription(string itemNumber)
        {
            return ItemDescription(GetItemData(itemNumber));
        }

        public string ItemDescription(Matricula item)
        {
            if (item != null)
            {
                if (Properties.Settings.Default.UsesItemDescription == 1)
                    return item.Denominacion;
                else if (Properties.Settings.Default.UsesItemDescription == 2)
                    return item.Denominacion2;
                else
                    return item.Denominacion + " " + item.Denominacion2;
            }
            return "";
        }

        public string ItemGroup(string itemNumber)
        {
            if (itemNumber != null)
            {
                Matricula item = GetItemData(itemNumber);
                if (item != null)
                    return item.TipoArticulo.Trim().ToUpper();
            }
            return "";
        }
    }
}
