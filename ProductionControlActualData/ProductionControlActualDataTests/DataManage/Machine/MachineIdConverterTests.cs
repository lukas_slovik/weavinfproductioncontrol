﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProductionControlActualData.DataManage.Machine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductionControlActualData.DataManage.Machine.Tests
{
    [TestClass()]
    public class MachineIdConverterTests
    {
        [TestMethod()]
        public void ConvertTestNull()
        {
            Assert.IsTrue(MachineIdConverter.Convert(null)==101);
        }

        [TestMethod()]
        public void ConvertTestEmpty()
        {
            Assert.IsTrue(MachineIdConverter.Convert("") == 101);
        }

        [TestMethod()]
        public void ConvertTest()
        {
            Assert.IsTrue(MachineIdConverter.Convert("103") == 103);
            Assert.IsTrue(MachineIdConverter.Convert("2*") == 101);
        }
    }
}